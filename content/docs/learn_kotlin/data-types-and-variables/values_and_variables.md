---
title: 'Values and variables'
order: 1
---

## What is a variable?

A variable is storage for a value which can be a string, a number, or something else. Every variable has a
**name** (or **identifier**) to distinguish it from other variables. You can access a value by the name of the variable.

Variables are one of the most often used elements of programs; therefore it is important to understand how to use them.

## Declaring variables

Before you can start using a variable, you must declare it. To declare a variable, Kotlin provides two keywords:

- `val` (from _value_) declares an **immutable variable** (just a **named value**, or a **constant**) that cannot be
  changed after it has been initialized. This is not entirely true, we will discuss this in more detail later;
- `var` (from _variable_) declares a **mutable variable** that can be changed (as many times as needed);

When you declare a variable, you must add its name after one of these keywords. Be careful: the name of a variable
cannot start with a digit. Usually, it starts with a letter. You should choose meaningful and readable names for
variables to make your code easy to understand.

To assign a certain value to a variable, we should use the assignment operator `=`.

Let's declare an immutable variable named `language` and initialize it with the string `"Kotlin"`.

```kotlin
val language = "Kotlin"
```

Now we can access this string by the variable's name. Let's print it!

```kotlin
println(language) // prints "Kotlin" without quotes
```

This variable cannot be modified after it has been initialized because it has been declared as `val`.

> The case of a name is important: `language` is not the same as `Language`.

Let's declare a mutable variable named `dayOfWeek` and print its value before and after changing it.

```kotlin
var dayOfWeek = "Monday"
println(dayOfWeek) // Monday

dayOfWeek = "Tuesday"
println(dayOfWeek) // Tuesday
```

In the example above, we declared a variable named `dayOfWeek` and initialized it with the value `"Monday"`. Then we
accessed the value by the variable name and printed it. After that, we changed the variable's value to `"Tuesday"`
and printed this new value.

> You do not need to declare a variable again to change its value. Just assign a new value to it using the `=` operator.

It is also possible to declare and initialize a variable with the value of another one:

```kotlin
val cost = 3
val costOfCoffee = cost
println(costOfCoffee) // 3
```

## Storing different types of values

Variables can store not only strings, but also numbers, characters, and other data types, which we will meet further.

Let's declare three immutable variables to store a number, a string, and a character, and then print their values.

```kotlin
val ten = 10
val greeting = "Hello"
val firstLetter = 'A'

println(ten) // 10
println(greeting) // Hello
println(firstLetter) // A
```

There is one restriction for mutable variables (which are declared with the keyword `var`). You can only reassign
values of the same type as the initial value. So, the piece of code below is not correct:

```kotlin
var number = 10
number = 11 // ok
number = "twelve" // an error here!
```

Please remember this restriction!

## Battle of keywords: val vs var

Now you know, there are two keywords that are used to declare variables. Actually, in many cases, it is better to use
immutable variables declared with the keyword `val`. Before using `var` you should make sure that `val` is not suitable
in that case. If it isn't, use `var`. Although keep in mind, that the more mutable variables in your program, the
harder it is to read. You know, immutable variables help write readable code. So, please, use `val` whenever possible!
You can easily replace it with `var` when you absolutely have to.
