---
title: 'Objects'
order: 7
---

## Everybody lies and everything is an object

You know, don't mind the first part of the title.

Lots of objects surround us in real life. You drive a car, watch TV, learn things, pay for bananas with your smartphone.
In Kotlin, every variable and value is an object. For example, an integer 5 and a string "high" are objects.
It's convenient, because programmers are people, and people are quite used to working with objects.
Now we are going to draw some parallels.

## Comparison

Imagine a situation: you receive two identical messages from your friend. The messages are `"Hi"` and `"Hi"`.
You see them, and you understand: the messages are the same. If you want to compare those messages in Kotlin,
you can store them as string values:

```kotlin
val msg1 = "Hi"
val msg2 = "Hi"
```

Then you can use the comparison operator `==`.
For example, `print(msg1 == msg2)` gives `true` and `print(msg1 == "Hello")` prints `false`.
For different types of objects, different relation operators are applied. It's just like in the real world.

## Copying by reference

Often, you can't copy an object. If you have ice cream, you can't duplicate it for free. The market shows no mercy.
Is there something you can do about it? Well, you can look in the mirror and see the reflection of your ice cream.
It looks just like the original one because technically it is.

In Kotlin, similar rules apply. Variables and values just point to objects. It means that if you create a variable
and assign an object to it, another variable can point to the same object as well.
Say you composed the text messages like this:

```kotlin
val msg1 = "Hi"
val msg2 = msg1
```

There will now be two values pointing to a single object `"Hi"`:

![Mirror](./assets/mirror.png)

In other words, the `=` sign doesn't copy the object itself, it only copies a reference to it.
This is an important thing to know because there are **mutable objects**: change them in one place,
and they will change everywhere. Standard types like strings or numbers are **immutable**, so it's safe to copy them
by reference. Later, you will learn about the mutable standard, custom types and other ways to make copies of objects.

## State and behavior

Objects can also have properties and they can do something. Let's use our messages as an example again.

First of all, a message has a state: it contains not only a sequence of symbols but also the size of the sequence,
that is, the length. Something that allows you to access the state of an object is called a **property** in Kotlin.
Just put a dot and write the name of the property after the object, and you will get what you want!
Look, there is a `.length` property in a string, so `msg1.length` gives us `2`.

Secondly, in Kotlin some **functions** are bound to the specific type.
This makes using the objects more logical because functions represent the behavior of those objects.
These functions are also called **member functions**. The syntax is similar – just put a dot.
Then we can repeat our message: `msg1.repeat(3)` gives us `"HiHiHi"`.

> How do we enumerate objects in real life? The point is that you use one object a time,
> not all the objects of the same kind simultaneously. For example, if you open a door,
> you don't open all the doors in the world. So, obviously not all the strings have a length of `2`.
> If you repeat a string `3` times, you won't get `"HiHiHi"` unless you repeat the `"Hi"` string.

## More about functions

Imagine we have an `obj` variable. You may have noticed that you can call some functions like this: `f(obj)`,
and others like that: `obj.f()`.

There is a difference between a function that receives an object and a function that is bound to an object.
It might be difficult to describe so let's use examples again.

The function that receives an object is `print`. It takes a string and prints it to the console.
There is not much relation between the console and a string, so the `print` function isn't bound to a string via a dot.

The function that is bound to a string is `.repeat`. It's the behavior of a string,
and it's a way to construct a new string. So it's reasonable to bind this function with a string, isn't it?

## Conclusion

Almost all objects in Kotlin work this way. They do so because they have a connection with real life.
Kotlin developers try to make the code clearer, that's why they use different syntax for functions.
